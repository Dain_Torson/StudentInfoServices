package com.dain_torson.dataprocessor;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XMLFileProcessor {

    private File file;
    private List<PersonDataContainer> students;

    public XMLFileProcessor(File file) {
        this.file = file;
    }

    private Element createStudentNode(PersonDataContainer source, Document document) {

        Element student = document.createElement("student");

        Element name = document.createElement("name");
        name.setTextContent(source.getFio());

        Element group = document.createElement("group");
        group.setTextContent(source.getGroup());

        Element eduForm = document.createElement("eduForm");
        eduForm.setTextContent(source.getEduForm());

        Element gender = document.createElement("gender");
        gender.setTextContent(source.getGender());

        Element city = document.createElement("city");
        city.setTextContent(source.getCity());

        Element age = document.createElement("age");
        age.setTextContent(String.valueOf(source.getAge()));

        Element grade = document.createElement("grade");
        grade.setTextContent(String.valueOf(source.getAvgGrade()));

        student.appendChild(name);
        student.appendChild(group);
        student.appendChild(eduForm);
        student.appendChild(gender);
        student.appendChild(city);
        student.appendChild(age);
        student.appendChild(grade);

        return student;
    }

    private void getStudentFromNode(Node source, PersonDataContainer target) {

        Element element = (Element) source;
        target.setFio(element.getElementsByTagName("name").item(0).getTextContent());
        target.setGroup(element.getElementsByTagName("group").item(0).getTextContent());
        target.setEduForm(element.getElementsByTagName("eduForm").item(0).getTextContent());
        target.setGender(element.getElementsByTagName("gender").item(0).getTextContent());
        target.setCity(element.getElementsByTagName("city").item(0).getTextContent());
        target.setAge(Integer.parseInt(element.getElementsByTagName("age").item(0).getTextContent()));
        target.setAvgGrade(Double.parseDouble(element.getElementsByTagName("grade").item(0).getTextContent()));
    }

    public void save(List<PersonDataContainer> students) {

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document document = docBuilder.newDocument();
            Element root = document.createElement("students");
            document.appendChild(root);

            for(PersonDataContainer student : students) {
                root.appendChild(createStudentNode(student, document));
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(file);

            transformer.transform(source, result);
        }
        catch (ParserConfigurationException exception) {
            exception.printStackTrace();
        }
        catch (TransformerConfigurationException exception) {
            exception.printStackTrace();
        }
        catch (TransformerException exception) {
            exception.printStackTrace();
        }
    }

    public void open(List<PersonDataContainer> target, PersonDataContainerCreator creator) {

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document document = docBuilder.parse(file);

            NodeList nodeList = document.getElementsByTagName("student");
            for(int nodeIdx= 0; nodeIdx < nodeList.getLength(); ++nodeIdx) {
                PersonDataContainer container = creator.create();
                getStudentFromNode(nodeList.item(nodeIdx), container);
                target.add(container);
            }
        }
        catch (ParserConfigurationException exception) {
            exception.printStackTrace();
        }
        catch (SAXException exception) {
            exception.printStackTrace();
        }
        catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
