package com.dain_torson.dataprocessor;

public interface PersonDataContainerCreator {
    PersonDataContainer create();
}
