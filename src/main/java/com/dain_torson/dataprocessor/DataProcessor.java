package com.dain_torson.dataprocessor;

import com.dain_torson.servlets.rpc.info.data.PersonData;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class DataProcessor {

    private PersonDataContainerCreator creator;
    private List<PersonDataContainer> persons;
    private String confPath = "conf.txt";
    private String dataPath;

    public DataProcessor(PersonDataContainerCreator creator){
        this.creator = creator;
        persons = new ArrayList<>();
        try {
            dataPath = retrieveDataPath();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        open();
    }

    private String retrieveDataPath() throws IOException{
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(confPath).getFile());
        return new Scanner(file).useDelimiter("\\Z").next();
    }

    private void open() {
        File file = new File(dataPath);
        if(file.exists()) {
            XMLFileProcessor fileProcessor = new XMLFileProcessor(file);
            fileProcessor.open(persons, creator);
        }
    }

    private void save() throws IOException{
        File file = new File(dataPath);
        Files.deleteIfExists(file.toPath());
        XMLFileProcessor fileProcessor = new XMLFileProcessor(file);
        fileProcessor.save(persons);
    }

    private boolean matchPattern(PersonDataContainer target, PersonDataContainer pattern) {
        return target.getFio().contains(pattern.getFio())
                && target.getGroup().contains(pattern.getGroup())
                && target.getEduForm().contains(pattern.getEduForm())
                && target.getGender().contains(pattern.getGender())
                && target.getCity().contains(pattern.getCity())
                && (target.getAge() == pattern.getAge() || pattern.getAge() == 0)
                && (target.getAvgGrade() == pattern.getAvgGrade()|| pattern.getAvgGrade() == 0);
    }

    public List<PersonDataContainer> getAll(){
        return new ArrayList<>(persons);
    }

    public List<PersonDataContainer> getByFio(String fio){
        return persons.stream().filter(person -> person.getFio().toLowerCase().contains(fio.toLowerCase()))
                        .collect(Collectors.toList());
    }

    public List<PersonDataContainer> getByPattern(PersonDataContainer pattern){
        return persons.stream().filter(person -> matchPattern(person, pattern))
                .collect(Collectors.toList());
    }

    public boolean newRecord(PersonDataContainer data){
        persons.add(data);
        try {
            save();
        }
        catch (IOException ex) {
            return false;
        }
        return true;
    }

    public boolean deleteRecord(PersonDataContainer pattern){
        List<PersonDataContainer> newPersons = persons.stream()
                .filter(person -> !matchPattern(person, pattern))
                .collect(Collectors.toList());
        persons = newPersons;
        try {
            save();
        }
        catch (IOException ex) {
            return false;
        }
        return true;
    }

    private PersonDataContainer copyData(PersonDataContainer source, PersonDataContainer target) {
        target.setFio(source.getFio());
        target.setGroup(source.getGroup());
        target.setGender(source.getGender());
        target.setAge(source.getAge());
        target.setCity(source.getCity());
        target.setAvgGrade(source.getAvgGrade());
        target.setEduForm(source.getEduForm());
        return target;
    }

    public boolean editRecord(PersonDataContainer oldData, PersonDataContainer newData){
        for(PersonDataContainer person : persons) {
            if(matchPattern(person, oldData)) {
                copyData(newData, person);
            }
        }
        try {
            save();
        }
        catch (IOException ex) {
            return false;
        }
        return true;
    }
}
