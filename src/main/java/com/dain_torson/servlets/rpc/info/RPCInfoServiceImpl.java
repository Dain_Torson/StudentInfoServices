package com.dain_torson.servlets.rpc.info;


import com.dain_torson.dataprocessor.DataProcessor;
import com.dain_torson.dataprocessor.PersonDataContainer;
import com.dain_torson.servlets.rpc.info.data.PersonData;
import com.dain_torson.servlets.rpc.info.data.PersonDataCreator;
import org.apache.thrift.TException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RPCInfoServiceImpl implements RPCInfoService.Iface{

    @Override
    public List<PersonData> getAll() throws TException {
        DataProcessor processor = new DataProcessor(new PersonDataCreator());
        List<PersonData> result = processor.getAll().stream().map(container -> (PersonData)container)
                                                    .collect(Collectors.toList());
        return result;
    }

    @Override
    public List<PersonData> getByFio(String fio) throws TException {
        DataProcessor processor = new DataProcessor(new PersonDataCreator());
        List<PersonData> result = processor.getByFio(fio).stream().map(container -> (PersonData)container)
                .collect(Collectors.toList());
        return result;
    }

    @Override
    public List<PersonData> getByPattern(PersonData pattern) throws TException {
        DataProcessor processor = new DataProcessor(new PersonDataCreator());
        List<PersonData> result = processor.getByPattern(pattern).stream().map(container -> (PersonData)container)
                .collect(Collectors.toList());
        return result;
    }

    @Override
    public boolean newRecord(PersonData data) throws TException {
        DataProcessor processor = new DataProcessor(new PersonDataCreator());
        return processor.newRecord(data);
    }

    @Override
    public boolean deleteRecord(PersonData pattern) throws TException {
        DataProcessor processor = new DataProcessor(new PersonDataCreator());
        return processor.deleteRecord(pattern);
    }

    @Override
    public boolean editRecord(PersonData oldData, PersonData newData) throws TException {
        DataProcessor processor = new DataProcessor(new PersonDataCreator());
        return processor.editRecord(oldData, newData);
    }
}
