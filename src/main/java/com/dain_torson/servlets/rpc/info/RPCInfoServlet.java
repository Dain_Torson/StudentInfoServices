package com.dain_torson.servlets.rpc.info;

import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.server.TServlet;

public class RPCInfoServlet extends TServlet {
    public RPCInfoServlet() {
        super(
                new RPCInfoService.Processor(
                        new RPCInfoServiceImpl()),
                new TCompactProtocol.Factory()
        );
    }
}
