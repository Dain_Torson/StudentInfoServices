package com.dain_torson.servlets.rpc.info.data;

import com.dain_torson.dataprocessor.PersonDataContainerCreator;

public class PersonDataCreator implements PersonDataContainerCreator {
    @Override
    public PersonData create() {
        return new PersonData();
    }
}
