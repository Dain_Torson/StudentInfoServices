package com.dain_torson.servlets.rpc.example;

public interface StringContainer {
    public String getStringObject();
}
