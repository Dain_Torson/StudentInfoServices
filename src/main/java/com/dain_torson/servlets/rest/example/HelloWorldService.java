package com.dain_torson.servlets.rest.example;

import com.dain_torson.dataprocessor.DataProcessor;
import com.dain_torson.servlets.rest.info.data.SimpleDataWrapper;
import com.dain_torson.servlets.soap.info.data.SimplePersonData;
import com.dain_torson.servlets.soap.info.data.SimplePersonDataCreator;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;
import java.util.stream.Collectors;

@Path("/hello")
public class HelloWorldService {
    @Context
    UriInfo uriInfo;
    @Context
    Request request;

    @GET
    @Path("/{param}")
    public Response getMessage(@PathParam("param") String message) {
        String output = "Jersey says " + message;
        return Response.status(200).entity(output).build();
    }

    @GET
    @Path("/get")
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public List<SimplePersonData> getAll() {
        DataProcessor processor = new DataProcessor(new SimplePersonDataCreator());
        SimpleDataWrapper dataWrapper = new SimpleDataWrapper();
        List<SimplePersonData> list = processor.getAll().stream().map(container -> (SimplePersonData) container)
                .collect(Collectors.toList());
        return list;
    }

    @POST
    @Path("/post")
    @Produces(MediaType.TEXT_XML)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public Response newRecord(SimpleDataWrapper data) {
        System.out.println(data.getFirst().getFio());
        System.out.println(data.getSecond().getFio());
        return Response.created(uriInfo.getAbsolutePath()).build();
    }
}