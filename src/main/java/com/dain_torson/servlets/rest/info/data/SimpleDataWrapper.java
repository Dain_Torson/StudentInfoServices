package com.dain_torson.servlets.rest.info.data;

import com.dain_torson.servlets.soap.info.data.SimplePersonData;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class SimpleDataWrapper {

    private SimplePersonData first;
    private SimplePersonData second;

    public SimplePersonData getFirst() {
        return first;
    }

    public void setFirst(SimplePersonData first) {
        this.first = first;
    }

    public SimplePersonData getSecond() {
        return second;
    }

    public void setSecond(SimplePersonData second) {
        this.second = second;
    }
}
