package com.dain_torson.servlets.rest.info;

import com.dain_torson.dataprocessor.DataProcessor;
import com.dain_torson.servlets.rest.info.data.SimpleDataWrapper;
import com.dain_torson.servlets.soap.info.data.SimplePersonData;
import com.dain_torson.servlets.soap.info.data.SimplePersonDataCreator;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;
import java.util.stream.Collectors;

@Path("/info")
public class RESTInfoService {
    @Context
    UriInfo uriInfo;
    @Context
    Request request;

    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public List<SimplePersonData> getAll() {
        DataProcessor processor = new DataProcessor(new SimplePersonDataCreator());
       return processor.getAll().stream().map(container -> (SimplePersonData) container)
                .collect(Collectors.toList());
    }

    @POST
    @Path("/getByFio")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_JSON)
    public List<SimplePersonData> getByFio(String fio) {
        DataProcessor processor = new DataProcessor(new SimplePersonDataCreator());
        return processor.getByFio(fio).stream().map(container -> (SimplePersonData) container)
                .collect(Collectors.toList());
    }

    @POST
    @Path("/getByPattern")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_JSON)
    public List<SimplePersonData> getByPattern(SimplePersonData pattern) {
        DataProcessor processor = new DataProcessor(new SimplePersonDataCreator());
        return processor.getByPattern(pattern).stream().map(container -> (SimplePersonData) container)
                .collect(Collectors.toList());
    }

    @POST
    @Path("/newRecord")
    @Produces(MediaType.TEXT_XML)
    @Consumes(MediaType.APPLICATION_XML)
    public Response newRecord(SimplePersonData data) {
        DataProcessor processor = new DataProcessor(new SimplePersonDataCreator());
        boolean success = processor.newRecord(data);
        if(!success) {
            return Response.notModified().build();
        }
        return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @POST
    @Path("/deleteRecord")
    @Produces(MediaType.TEXT_XML)
    @Consumes(MediaType.APPLICATION_XML)
    public Response deleteRecord(SimplePersonData data) {
        DataProcessor processor = new DataProcessor(new SimplePersonDataCreator());
        boolean success = processor.deleteRecord(data);
        if(!success) {
            return Response.notModified().build();
        }
        return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @POST
    @Path("/editRecord")
    @Produces(MediaType.TEXT_XML)
    @Consumes(MediaType.APPLICATION_XML)
    public Response editRecord(SimpleDataWrapper data) {
        DataProcessor processor = new DataProcessor(new SimplePersonDataCreator());
        boolean success = processor.editRecord(data.getFirst(), data.getSecond());
        if(!success) {
            return Response.notModified().build();
        }
        return Response.created(uriInfo.getAbsolutePath()).build();
    }

}
