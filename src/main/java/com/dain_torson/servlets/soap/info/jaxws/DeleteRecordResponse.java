
package com.dain_torson.servlets.soap.info.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "deleteRecordResponse", namespace = "http://info.soap.servlets.dain_torson.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteRecordResponse", namespace = "http://info.soap.servlets.dain_torson.com/")
public class DeleteRecordResponse {

    @XmlElement(name = "return", namespace = "")
    private boolean _return;

    /**
     * 
     * @return
     *     returns boolean
     */
    public boolean isReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(boolean _return) {
        this._return = _return;
    }

}
