package com.dain_torson.servlets.soap.info.data;

import com.dain_torson.dataprocessor.PersonDataContainerCreator;

public class SimplePersonDataCreator implements PersonDataContainerCreator {
    @Override
    public SimplePersonData create() {
        return new SimplePersonData();
    }
}
