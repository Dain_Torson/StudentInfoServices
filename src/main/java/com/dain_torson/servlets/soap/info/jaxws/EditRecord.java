
package com.dain_torson.servlets.soap.info.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "editRecord", namespace = "http://info.soap.servlets.dain_torson.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "editRecord", namespace = "http://info.soap.servlets.dain_torson.com/", propOrder = {
    "arg0",
    "arg1"
})
public class EditRecord {

    @XmlElement(name = "arg0", namespace = "")
    private com.dain_torson.servlets.soap.info.data.SimplePersonData arg0;
    @XmlElement(name = "arg1", namespace = "")
    private com.dain_torson.servlets.soap.info.data.SimplePersonData arg1;

    /**
     * 
     * @return
     *     returns SimplePersonData
     */
    public com.dain_torson.servlets.soap.info.data.SimplePersonData getArg0() {
        return this.arg0;
    }

    /**
     * 
     * @param arg0
     *     the value for the arg0 property
     */
    public void setArg0(com.dain_torson.servlets.soap.info.data.SimplePersonData arg0) {
        this.arg0 = arg0;
    }

    /**
     * 
     * @return
     *     returns SimplePersonData
     */
    public com.dain_torson.servlets.soap.info.data.SimplePersonData getArg1() {
        return this.arg1;
    }

    /**
     * 
     * @param arg1
     *     the value for the arg1 property
     */
    public void setArg1(com.dain_torson.servlets.soap.info.data.SimplePersonData arg1) {
        this.arg1 = arg1;
    }

}
