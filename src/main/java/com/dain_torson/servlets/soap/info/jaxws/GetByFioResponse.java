
package com.dain_torson.servlets.soap.info.jaxws;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getByFioResponse", namespace = "http://info.soap.servlets.dain_torson.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getByFioResponse", namespace = "http://info.soap.servlets.dain_torson.com/")
public class GetByFioResponse {

    @XmlElement(name = "return", namespace = "")
    private List<com.dain_torson.servlets.soap.info.data.SimplePersonData> _return;

    /**
     * 
     * @return
     *     returns List<SimplePersonData>
     */
    public List<com.dain_torson.servlets.soap.info.data.SimplePersonData> getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(List<com.dain_torson.servlets.soap.info.data.SimplePersonData> _return) {
        this._return = _return;
    }

}
