package com.dain_torson.servlets.soap.info;


import com.dain_torson.dataprocessor.DataProcessor;
import com.dain_torson.servlets.soap.info.data.SimplePersonData;
import com.dain_torson.servlets.soap.info.data.SimplePersonDataCreator;

import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@WebService(endpointInterface = "com.dain_torson.servlets.soap.info.SOAPInfoService")
public class SOAPInfoServiceImpl implements SOAPInfoService{

    @Override
    public List<SimplePersonData> getAll() {
        DataProcessor processor = new DataProcessor(new SimplePersonDataCreator());
        return processor.getAll().stream().map(container -> (SimplePersonData)container)
                .collect(Collectors.toList());

    }

    @Override
    public List<SimplePersonData> getByFio(String fio) {
        DataProcessor processor = new DataProcessor(new SimplePersonDataCreator());
        return processor.getByFio(fio).stream().map(container -> (SimplePersonData)container)
                .collect(Collectors.toList());
    }

    @Override
    public List<SimplePersonData> getByPattern(SimplePersonData pattern) {
        DataProcessor processor = new DataProcessor(new SimplePersonDataCreator());
        return processor.getByPattern(pattern).stream().map(container -> (SimplePersonData)container)
                .collect(Collectors.toList());
    }

    @Override
    public boolean newRecord(SimplePersonData data) {
        DataProcessor processor = new DataProcessor(new SimplePersonDataCreator());
        return processor.newRecord(data);
    }

    @Override
    public boolean deleteRecord(SimplePersonData pattern) {
        DataProcessor processor = new DataProcessor(new SimplePersonDataCreator());
        return processor.deleteRecord(pattern);
    }

    @Override
    public boolean editRecord(SimplePersonData oldData, SimplePersonData newData) {
        DataProcessor processor = new DataProcessor(new SimplePersonDataCreator());
        return processor.editRecord(oldData, newData);
    }
}
