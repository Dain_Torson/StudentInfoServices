
package com.dain_torson.servlets.soap.info.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getAll", namespace = "http://info.soap.servlets.dain_torson.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAll", namespace = "http://info.soap.servlets.dain_torson.com/")
public class GetAll {


}
