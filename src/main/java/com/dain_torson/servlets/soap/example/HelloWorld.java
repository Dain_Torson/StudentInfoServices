package com.dain_torson.servlets.soap.example;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import java.util.List;

//Service Endpoint Interface
@WebService
@SOAPBinding(style = Style.DOCUMENT, use= SOAPBinding.Use.LITERAL) //optional
public interface HelloWorld{

    @WebMethod
    List<DummyContainer> getHelloWorldAsString(String name);
}
