package com.dain_torson.servlets.soap.example;

public class DummyContainer implements Container{

    private String someString;

    public String getSomeString() {
        return someString;
    }

    public Container setSomeString(String someString) {
        this.someString = someString;
        return this;
    }
}
