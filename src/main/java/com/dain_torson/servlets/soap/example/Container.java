package com.dain_torson.servlets.soap.example;

public interface Container {
    public String getSomeString();
    public Container setSomeString(String someString);
}
