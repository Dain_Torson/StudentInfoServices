package com.dain_torson.servlets.soap.example;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

//Service Implementation
@WebService(endpointInterface = "com.dain_torson.servlets.soap.example.HelloWorld")
public class HelloWorldImp implements HelloWorld{

    @Override
    public List<DummyContainer> getHelloWorldAsString(String name) {
        List<DummyContainer> list = new ArrayList<>();
        DummyContainer container1 = new DummyContainer();
        container1.setSomeString("Hello Ales!");
        list.add(container1);
        DummyContainer container2 = new DummyContainer();
        container2.setSomeString("Hello again!");
        list.add(container2);
        return list;
    }
}
