namespace java com.dain_torson.servlets.rpc.info

struct PersonData {
        1: string fio;
        2: string group;
        3: string eduForm;
        4: string gender;
        5: string city;
        6: i32 age;
        7: double avgGrade;
}

service RPCInfoService {
  list<PersonData> getAll(),
  list<PersonData> getByFio(1: string fio),
  list<PersonData> getByPattern(1: PersonData pattern),
  bool newRecord(1: PersonData data),
  bool deleteRecord(1: PersonData pattern),
  bool editRecord(1: PersonData oldData; 2: PersonData newData)
}